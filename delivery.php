<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dtc";
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); } 

?><!DOCTYPE html>

<html>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

<style type="text/css">
	*{ margin: 0; padding: 0; }
	html{ background-color: #EAEDED; }
	body { width: 765px; margin: 0 auto; background-color: #FFFFFF; padding: 10px;}
	.menu ul { list-style-type: none; }
	.menu li { display: inline-block; padding: 0 10px; cursor: pointer; text-decoration: underline; }

	table{ width: 100%; }
	th,td {padding: 5px; text-align: center;}
</style>


<?php


if(isset($_POST['messenger'])){
	$sql = "UPDATE order_request SET messenger='".$_POST['messenger']."' where ref='".$_POST['ref']."' ";
	if ($conn->query($sql) === TRUE) { $msg = "บันทึกสำเร็จ"; }
	else {echo "Error: " . $sql . "<br>" . $conn->error;}
}


$sql = "select * from order_request";
$data = $conn->query($sql);

$sql2 = "select * from messenger";
$data2 = $conn->query($sql2);
?>



<body>

<div class="menu">
	<ul>
		<li><a href="order.php">เพิ่มออเดอร์</a></li>
		<li><a href="delivery.php">จัดส่ง</a></li>
		<li><a href="report.php">รายงาน</a></li>
	</ul>
</div>


<h1>รายการจัดส่ง</h1>


<div>
	
	<table border="1" collapse="collapse" >
		<tr>
			<th>No.</th>
			<th>Ref No.</th>
			<th>วันที่จัดส่ง</th>
			<th>น้ำหนักรวม (ก.ก.)</th>
			<th>เจ้าหน้าที่จัดส่ง</th>
			<th></th>
		</tr>
		<?php
			foreach ($data as $key => $val) {
		?>
				<tr>
					<td><?= $key+1 ?></td>
					<td><?= $val['ref'] ?></td>
					<td><?= $val['delivery'] ?></td>
					<td><?= $val['sum_weight'] ?></td>
					<td>
						<?php if($val['messenger']){ ?>
							<select name="messenger" id="<?= $val['ref'] ?>_sel" disabled="disabled">
								<option value="0">--เลือกผู้จัดส่ง--</option>
								<?php foreach ($data2 as $val2) { ?>
									<option value="<?= $val2['id'] ?>"  <?php if($val['messenger']==$val2['id']){ ?> selected="selected" <?php } ?> ><?= $val2['name'] ?></option>
								<?php } ?>
							</select>
						<?php }else{ ?>
							<form action="" method="post" id="<?= $val['ref'] ?>_form">
								<input type="hidden" name="ref" value="<?= $val['ref'] ?>">
								<select name="messenger" id="<?= $val['ref'] ?>_sel">
									<option value="0">--เลือกผู้จัดส่ง--</option>
									<?php foreach ($data2 as $val2) { ?>
										<option value="<?= $val2['id'] ?>"><?= $val2['name'] ?></option>
									<?php } ?>
								</select>
							</form>
						<?php } ?>
					</td>
					<td>
						<?php if($val['messenger']){ ?>
							<input type="button" value="Edit" class="btn" data-ref="<?= $val['ref'] ?>">
						<?php }else{ ?>
							<input type="button" value="Save" class="btn" data-ref="<?= $val['ref'] ?>">
						<?php } ?>
					</td>
				</tr>
		<?php
			}
		?>
	</table>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
	$('.btn').click(function(){
		var ref = this.dataset.ref;
		if($('#'+ref+'_sel').val()){
			$('#'+ref+'_form').submit();
		}
	})
</script>

</body>
</html>