-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2021 at 07:22 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dtc`
--

-- --------------------------------------------------------

--
-- Table structure for table `messenger`
--

CREATE TABLE `messenger` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messenger`
--

INSERT INTO `messenger` (`id`, `name`, `rate`) VALUES
(1, 'นายส้ม', 60),
(2, 'นายมะละกอ', 50),
(3, 'นายฝรั่ง', 40);

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `weight` float NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id`, `ref_id`, `name`, `weight`, `amount`) VALUES
(1, 'HQFVJB', 'หญ้าอัลฟาก้า', 1, 3),
(2, 'HQFVJB', 'อาหารเม็ดสำหรับกระต่าย', 2.25, 1),
(3, 'JKRNIY', 'หญ้าธิโมธี', 1, 6),
(4, 'JKRNIY', 'อาหารเม็ดสำหรับกระต่าย', 3, 2),
(5, 'YSCXZF', 'อาหารเม็ดสำหรับกระต่าย', 2, 1),
(6, 'YSCXZF', 'นมแพะอัดเม็ด', 1, 1),
(7, 'TGCPUO', 'หญ้าอัลฟาก้า', 2, 1),
(8, 'TGCPUO', 'หญ้าธิโมธี', 2, 1),
(9, 'TGCPUO', 'อาหารเม็ดสำหรับกระต่าย', 2, 1),
(10, 'TGCPUO', 'นมแพะอัดเม็ด', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_request`
--

CREATE TABLE `order_request` (
  `id` int(11) NOT NULL,
  `ref` varchar(100) NOT NULL,
  `delivery` datetime NOT NULL,
  `sum_weight` float NOT NULL,
  `messenger` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_request`
--

INSERT INTO `order_request` (`id`, `ref`, `delivery`, `sum_weight`, `messenger`) VALUES
(1, 'HQFVJB', '2021-02-24 12:00:00', 5.25, 0),
(2, 'JKRNIY', '2021-02-24 14:00:00', 12, 0),
(3, 'YSCXZF', '2021-02-25 10:00:00', 3, 0),
(4, 'TGCPUO', '2021-02-25 14:00:00', 7, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messenger`
--
ALTER TABLE `messenger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_id` (`ref_id`);

--
-- Indexes for table `order_request`
--
ALTER TABLE `order_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref` (`ref`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messenger`
--
ALTER TABLE `messenger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `order_request`
--
ALTER TABLE `order_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
