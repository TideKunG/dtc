<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dtc";
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); } 

?><!DOCTYPE html>

<html>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

<style type="text/css">
	*{ margin: 0; padding: 0; }
	html{ background-color: #EAEDED; }
	body { width: 765px; margin: 0 auto; background-color: #FFFFFF; padding: 10px;}
	.menu ul { list-style-type: none; }
	.menu li { display: inline-block; padding: 0 10px; cursor: pointer; text-decoration: underline; }

	table{ width: 100%; }
	th,td {padding: 5px; text-align: center;}

	.search .form-control{ display: inline-block; width: 200px; margin-bottom: 3px; }
</style>


<?php

if(isset($_POST)){
	if($_POST['messenger']) {
		$sql = "select m.name,r.delivery,sum(r.sum_weight) as sum_w,count(r.ref) as sum_order, sum(r.sum_weight)*m.rate as rate from order_request r left join messenger m on r.messenger = m.id where r.messenger=".$_POST['messenger']." group by r.messenger";
	}

	if($_POST['date']) {
		$sql = "select m.name,r.delivery,sum(r.sum_weight) as sum_w,count(r.ref) as sum_order, sum(r.sum_weight)*m.rate as rate from order_request r left join messenger m on r.messenger = m.id where DATE(r.delivery)='".$_POST['date']."' group by DATE(r.delivery)";
	}

	if($_POST['product']) {
		$sql = "select m.name,r.delivery,sum(r.sum_weight) as sum_w,count(r.ref) as sum_order, sum(r.sum_weight)*m.rate as rate from order_request r left join messenger m on r.messenger = m.id left join order_detail d on d.ref_id=r.ref where d.name='".$_POST['product']."' group by r.messenger,DATE(r.delivery)";
	}
}
$data = $conn->query($sql);


$sql2 = "select * from messenger";
$data2 = $conn->query($sql2);
?>



<body>

<div class="menu">
	<ul>
		<li><a href="order.php">เพิ่มออเดอร์</a></li>
		<li><a href="delivery.php">จัดส่ง</a></li>
		<li><a href="report.php">รายงาน</a></li>
	</ul>
</div>


<h1>รายงาน</h1>


<div>

	<div class="search">
		<h3>ค้นหา</h3>
		<form method="post">
			<div>ผู้จัดส่ง : 
				<select name="messenger" id="<?= $val['ref'] ?>_sel" class="form-control">
					<option value="0">--เลือกผู้จัดส่ง--</option>
					<?php foreach ($data2 as $val2) { ?>
						<option value="<?= $val2['id'] ?>"><?= $val2['name'] ?></option>
					<?php } ?>
				</select>
			</div>
			<div>วันที่จัดส่ง : <input type="text" name="date" id="dateP" class="form-control"></div>
			<div>ชื่อสินค้า : <input type="text" name="product" class="form-control"></div>
			<input type="submit" value="Search" class="btn">
		</form>
	</div>
	<br><br>
	
	<table border="1" collapse="collapse" >
		<tr>
			<th>No.</th>
			<th>ชื่อผู้จัดส่ง</th>
			<th>วันที่จัดส่ง</th>
			<th>จำนวนออเดอร์</th>
			<th>น้ำหนักรวมที่จัดส่ง</th>
			<th>ค่าจัดส่ง</th>
		</tr>
		<?php
			foreach ($data as $key => $val) {
		?>
				<tr>
					<td><?= $key+1 ?></td>
					<td><?= $val['name'] ?></td>
					<td><?= date("d/m/yy", strtotime($val['delivery'])) ?></td>
					<td><?= $val['sum_order'] ?></td>
					<td><?= $val['sum_w'] ?></td>
					<td><?= $val['rate'] ?></td>
				</tr>
		<?php
			}
		?>
	</table>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
</script>

</body>
</html>