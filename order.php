<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dtc";
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); } 

?><!DOCTYPE html>

<html>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

<style type="text/css">
	*{ margin: 0; padding: 0; }
	html{ background-color: #EAEDED; }
	body { width: 765px; margin: 0 auto; background-color: #FFFFFF; padding: 10px;}
	.col{ display: inline-block; width: 180px; }
	#datetimepicker1 { width: 50%; }
	.menu ul { list-style-type: none; }
	.menu li { display: inline-block; padding: 0 10px; cursor: pointer; text-decoration: underline; }
</style>


<?php
$msg = '';
$sum_w = 0;
//echo print_r($_POST,true);

if(isset($_POST['delivery'])){
	$ref = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), -6);
	$sql = "INSERT INTO order_request (ref, delivery) VALUES ('$ref','".$_POST['delivery']."')";
	if ($conn->query($sql) === TRUE) { $msg = "บันทึกสำเร็จ"; $last_id = $conn->insert_id; }
	else {echo "Error: " . $sql . "<br>" . $conn->error;}


	foreach ($_POST['name'] as $key => $value) { if(!$value) continue;
		$sum_w += (floatval($_POST['weight'][$key])*intval($_POST['amount'][$key]));
		$sql2 = "INSERT INTO order_detail (ref_id, name, weight, amount) VALUES ('$ref','".$_POST['name'][$key]."','".$_POST['weight'][$key]."', '".$_POST['amount'][$key]."')";
		if ($conn->query($sql2) === FALSE) { $msg = "บันทึกไม่สำเร็จ"; echo $conn->error;  }
	}


	$sql = "UPDATE order_request SET sum_weight=$sum_w where ref='$ref' ";
	if ($conn->query($sql) === TRUE) { $msg = "บันทึกสำเร็จ"; }

	//echo $ref.'--'.$msg.'<br>';
}

?>



<body>

<div class="menu">
	<ul>
		<li><a href="order.php">เพิ่มออเดอร์</a></li>
		<li><a href="delivery.php">จัดส่ง</a></li>
		<li><a href="report.php">รายงาน</a></li>
	</ul>
</div>


<h1>เพิ่มออเดอร์</h1>


<div>
	<div class="msg"><?= $msg ?></div>
	<form action="" id="target" method="post">
		<div>วัน-เวลาจัดส่ง : 
			<div class='input-group date' id='datetimepicker1'>
				<input type='text' name="delivery" class="form-control" />
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
		</div>
		<br>
		<div class="title">
			<div class="col">รายการ</div>
			<div class="col">น้ำหนัก (ก.ก.)</div>
			<div class="col">จำนวน</div>
		</div>
		<div class="or_form">
			<div class="or_input"> 
				<div class="col"><input type="text" class="form-control" name="name[]"></div>
				<div class="col"><input type="text" class="form-control" name="weight[]"></div>
				<div class="col"><input type="text" class="form-control" name="amount[]"></div>
			</div>
		</div>
		<input type="submit" class="btn" value="Add" id="add"><br><br>
		<input type="button" class="btn" value="Submit" id="btnsave">
	</form>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>


<script type="text/javascript">

	$('#btnsave').click(function(e) { $('#target').submit(); });

	$('#add').on('click submit', function(e) {
		e.preventDefault();
		$( ".or_input" ).clone().appendTo( ".or_form" ).removeClass("or_input").find("input").val('');
	});


$(function() {
  $('#datetimepicker1').datetimepicker({ format: 'YYYY-MM-DD HH:mm:ss' });
});
</script>

</body>
</html>